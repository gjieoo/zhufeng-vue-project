import axios from 'axios';
import config from '@/config';
class HttpRequest{
  constructor(){
    this.baseURL = config.baseURL;
    this.timeout = 3000;
  }
  setInterceptors(instance){
    instance.interceptors.request.use((config)=>{
      config.headers.authorization = 'Bearer ' + getLocal('token')
      return config;
    });
    instance.interceptors.response.use(res => {
      if (res && res.status == 200) {
          // 服务返回的结果都会放到data中
          if(res.data.err === 0){ // 统一处理错误状态码
              console.log('res',res.data)
              return Promise.resolve(res.data);//成功状态
          }else{
              return Promise.reject(res.data.data);//失败状态
          }
      } else {
          return Promise.reject(res.data.data); // 我的后端实现的话 如果失败了会在返回的结果中增加一个data字段 失败的原因
      }
  }, err => {
      // 单独处理其他的状态码异常
      switch (err.response.status) {
          case '401':
              console.log(err);
              break;
          default:
              break;
      }
      return Promise.reject(err);
  });
  }
  mergeOptions(options){
    return {
      baseURL:this.baseURL,
      timeout:this.timeout,
      ...options
    }
  }
  request(options){
    const instance = axios.create();
    this.setInterceptors(instance);
    const opts = this.mergeOptions(options);
    return instance(opts);
  }
  get(url,config){
    return this.request({
      method:'get',
      url,
      ...config
    });
  }
  post(url,data){
    return this.request({
      method:'post',
      url,
      data
    });
  }
}

export default new HttpRequest;