import {getLocal} from '@/utils/local';
import store from '../store';
import * as types from '@/store/action-types';

class WS {
  constructor(config={}){
    this.url = config.url || 'vue-zhufengpeixun.cn';
    this.port = config.port || 80;
    this.protocol = config.protocol || 'ws';
    this.time = config.time || 30*1000;
    this.ws = null;
  }

  onOpen = ()=>{
    this.ws.send(JSON.stringify({
      type:'auth',
      data:getLocal('token')
    }))
  }

  onMessage = (e) => {
    let {type,data} = JSON.parse(e.data);

    switch(type){
      case 'noAuth':
        console.log('没有权限');
        break;
      case 'heartCheck':
        this.checkServer();
        this.ws.send(JSON.stringify({type:'heartCheck'}));
        break;
      default:
        store.commit(types.SET_Message,data);
        break;
    }
  }

  onError = () => {
    setTimeout(()=>{
      this.create();
    },1000);
  }

  onClose = () => {
    this.ws.close();
  }

  create(){
    this.ws = new WebSoceket(`${this.protocol}://${this.url}:${this.port}`);
    this.ws.onopen = this.onOpen;
    this.ws.onmessage = this.onMessage;
    this.ws.onclose = this.onclose;
    this.ws.onerror = this.onError
  }

  checkServer(){//服务器回应
    clearTimeout(this.timer); // 防抖
    this.timer = setTimeout(() => {
      this.onClose();
      this.onError();
    }, this.time + 1000); //断线重连
  }

  send = (msg)=>{//发送信息   src/views/manager/infoPublish.vue中
    //  <el-input placeholder="请输入要发送给用户的信息" v-model="msg"></el-input>就会接受到发送的信息
    this.ws.send(JSON.stringify(msg))//在Network上可以看到发送的信息
  }
}

export default WS;